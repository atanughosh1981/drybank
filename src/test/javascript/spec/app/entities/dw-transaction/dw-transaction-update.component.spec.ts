/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { DrybankTestModule } from '../../../test.module';
import { DWTransactionUpdateComponent } from 'app/entities/dw-transaction/dw-transaction-update.component';
import { DWTransactionService } from 'app/entities/dw-transaction/dw-transaction.service';
import { DWTransaction } from 'app/shared/model/dw-transaction.model';

describe('Component Tests', () => {
    describe('DWTransaction Management Update Component', () => {
        let comp: DWTransactionUpdateComponent;
        let fixture: ComponentFixture<DWTransactionUpdateComponent>;
        let service: DWTransactionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [DrybankTestModule],
                declarations: [DWTransactionUpdateComponent]
            })
                .overrideTemplate(DWTransactionUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DWTransactionUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DWTransactionService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DWTransaction(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.dWTransaction = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DWTransaction();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.dWTransaction = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
