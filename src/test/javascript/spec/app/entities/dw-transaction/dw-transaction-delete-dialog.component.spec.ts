/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { DrybankTestModule } from '../../../test.module';
import { DWTransactionDeleteDialogComponent } from 'app/entities/dw-transaction/dw-transaction-delete-dialog.component';
import { DWTransactionService } from 'app/entities/dw-transaction/dw-transaction.service';

describe('Component Tests', () => {
    describe('DWTransaction Management Delete Component', () => {
        let comp: DWTransactionDeleteDialogComponent;
        let fixture: ComponentFixture<DWTransactionDeleteDialogComponent>;
        let service: DWTransactionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [DrybankTestModule],
                declarations: [DWTransactionDeleteDialogComponent]
            })
                .overrideTemplate(DWTransactionDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DWTransactionDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DWTransactionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
