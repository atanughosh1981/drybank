/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DrybankTestModule } from '../../../test.module';
import { DWTransactionDetailComponent } from 'app/entities/dw-transaction/dw-transaction-detail.component';
import { DWTransaction } from 'app/shared/model/dw-transaction.model';

describe('Component Tests', () => {
    describe('DWTransaction Management Detail Component', () => {
        let comp: DWTransactionDetailComponent;
        let fixture: ComponentFixture<DWTransactionDetailComponent>;
        const route = ({ data: of({ dWTransaction: new DWTransaction(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [DrybankTestModule],
                declarations: [DWTransactionDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(DWTransactionDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DWTransactionDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.dWTransaction).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
