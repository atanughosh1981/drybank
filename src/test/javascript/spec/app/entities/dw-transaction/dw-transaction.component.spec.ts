/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { DrybankTestModule } from '../../../test.module';
import { DWTransactionComponent } from 'app/entities/dw-transaction/dw-transaction.component';
import { DWTransactionService } from 'app/entities/dw-transaction/dw-transaction.service';
import { DWTransaction } from 'app/shared/model/dw-transaction.model';

describe('Component Tests', () => {
    describe('DWTransaction Management Component', () => {
        let comp: DWTransactionComponent;
        let fixture: ComponentFixture<DWTransactionComponent>;
        let service: DWTransactionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [DrybankTestModule],
                declarations: [DWTransactionComponent],
                providers: []
            })
                .overrideTemplate(DWTransactionComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DWTransactionComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DWTransactionService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new DWTransaction(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.dWTransactions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
