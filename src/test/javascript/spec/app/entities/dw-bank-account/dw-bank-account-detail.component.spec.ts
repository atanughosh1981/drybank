/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DrybankTestModule } from '../../../test.module';
import { DWBankAccountDetailComponent } from 'app/entities/dw-bank-account/dw-bank-account-detail.component';
import { DWBankAccount } from 'app/shared/model/dw-bank-account.model';

describe('Component Tests', () => {
    describe('DWBankAccount Management Detail Component', () => {
        let comp: DWBankAccountDetailComponent;
        let fixture: ComponentFixture<DWBankAccountDetailComponent>;
        const route = ({ data: of({ dWBankAccount: new DWBankAccount(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [DrybankTestModule],
                declarations: [DWBankAccountDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(DWBankAccountDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(DWBankAccountDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.dWBankAccount).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
