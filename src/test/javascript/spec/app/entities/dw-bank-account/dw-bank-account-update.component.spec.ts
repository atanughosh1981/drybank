/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { DrybankTestModule } from '../../../test.module';
import { DWBankAccountUpdateComponent } from 'app/entities/dw-bank-account/dw-bank-account-update.component';
import { DWBankAccountService } from 'app/entities/dw-bank-account/dw-bank-account.service';
import { DWBankAccount } from 'app/shared/model/dw-bank-account.model';

describe('Component Tests', () => {
    describe('DWBankAccount Management Update Component', () => {
        let comp: DWBankAccountUpdateComponent;
        let fixture: ComponentFixture<DWBankAccountUpdateComponent>;
        let service: DWBankAccountService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [DrybankTestModule],
                declarations: [DWBankAccountUpdateComponent]
            })
                .overrideTemplate(DWBankAccountUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DWBankAccountUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DWBankAccountService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DWBankAccount(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.dWBankAccount = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new DWBankAccount();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.dWBankAccount = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
