/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { DrybankTestModule } from '../../../test.module';
import { DWBankAccountComponent } from 'app/entities/dw-bank-account/dw-bank-account.component';
import { DWBankAccountService } from 'app/entities/dw-bank-account/dw-bank-account.service';
import { DWBankAccount } from 'app/shared/model/dw-bank-account.model';

describe('Component Tests', () => {
    describe('DWBankAccount Management Component', () => {
        let comp: DWBankAccountComponent;
        let fixture: ComponentFixture<DWBankAccountComponent>;
        let service: DWBankAccountService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [DrybankTestModule],
                declarations: [DWBankAccountComponent],
                providers: []
            })
                .overrideTemplate(DWBankAccountComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(DWBankAccountComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DWBankAccountService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new DWBankAccount(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.dWBankAccounts[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
