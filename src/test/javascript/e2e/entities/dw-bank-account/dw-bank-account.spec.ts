import { browser } from 'protractor';
import { NavBarPage } from './../../page-objects/jhi-page-objects';
import { DWBankAccountComponentsPage, DWBankAccountUpdatePage } from './dw-bank-account.page-object';

describe('DWBankAccount e2e test', () => {
    let navBarPage: NavBarPage;
    let dWBankAccountUpdatePage: DWBankAccountUpdatePage;
    let dWBankAccountComponentsPage: DWBankAccountComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load DWBankAccounts', () => {
        navBarPage.goToEntity('dw-bank-account');
        dWBankAccountComponentsPage = new DWBankAccountComponentsPage();
        expect(dWBankAccountComponentsPage.getTitle()).toMatch(/DW Bank Accounts/);
    });

    it('should load create DWBankAccount page', () => {
        dWBankAccountComponentsPage.clickOnCreateButton();
        dWBankAccountUpdatePage = new DWBankAccountUpdatePage();
        expect(dWBankAccountUpdatePage.getPageTitle()).toMatch(/Create or edit a DW Bank Account/);
        dWBankAccountUpdatePage.cancel();
    });

    it('should create and save DWBankAccounts', () => {
        dWBankAccountComponentsPage.clickOnCreateButton();
        dWBankAccountUpdatePage.setUsernameInput('username');
        expect(dWBankAccountUpdatePage.getUsernameInput()).toMatch('username');
        dWBankAccountUpdatePage.setPasswordInput('password');
        expect(dWBankAccountUpdatePage.getPasswordInput()).toMatch('password');
        dWBankAccountUpdatePage.setFirstNameInput('firstName');
        expect(dWBankAccountUpdatePage.getFirstNameInput()).toMatch('firstName');
        dWBankAccountUpdatePage.setLastNameInput('lastName');
        expect(dWBankAccountUpdatePage.getLastNameInput()).toMatch('lastName');
        dWBankAccountUpdatePage.setEmailInput('email');
        expect(dWBankAccountUpdatePage.getEmailInput()).toMatch('email');
        dWBankAccountUpdatePage.setPhoneInput('phone');
        expect(dWBankAccountUpdatePage.getPhoneInput()).toMatch('phone');
        dWBankAccountUpdatePage.setAddressInput('address');
        expect(dWBankAccountUpdatePage.getAddressInput()).toMatch('address');
        dWBankAccountUpdatePage.setPostalCodeInput('postalCode');
        expect(dWBankAccountUpdatePage.getPostalCodeInput()).toMatch('postalCode');
        dWBankAccountUpdatePage.setPanNumberInput('panNumber');
        expect(dWBankAccountUpdatePage.getPanNumberInput()).toMatch('panNumber');
        dWBankAccountUpdatePage.save();
        expect(dWBankAccountUpdatePage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});
