import { browser } from 'protractor';
import { NavBarPage } from './../../page-objects/jhi-page-objects';
import { DWTransactionComponentsPage, DWTransactionUpdatePage } from './dw-transaction.page-object';

describe('DWTransaction e2e test', () => {
    let navBarPage: NavBarPage;
    let dWTransactionUpdatePage: DWTransactionUpdatePage;
    let dWTransactionComponentsPage: DWTransactionComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load DWTransactions', () => {
        navBarPage.goToEntity('dw-transaction');
        dWTransactionComponentsPage = new DWTransactionComponentsPage();
        expect(dWTransactionComponentsPage.getTitle()).toMatch(/DW Transactions/);
    });

    it('should load create DWTransaction page', () => {
        dWTransactionComponentsPage.clickOnCreateButton();
        dWTransactionUpdatePage = new DWTransactionUpdatePage();
        expect(dWTransactionUpdatePage.getPageTitle()).toMatch(/Create or edit a DW Transaction/);
        dWTransactionUpdatePage.cancel();
    });

    it('should create and save DWTransactions', () => {
        dWTransactionComponentsPage.clickOnCreateButton();
        dWTransactionUpdatePage.setTransactionDateInput('2000-12-31');
        expect(dWTransactionUpdatePage.getTransactionDateInput()).toMatch('2000-12-31');
        dWTransactionUpdatePage.setTransactionRemarkInput('transactionRemark');
        expect(dWTransactionUpdatePage.getTransactionRemarkInput()).toMatch('transactionRemark');
        dWTransactionUpdatePage.transactionTypeSelectLastOption();
        dWTransactionUpdatePage.setAvailableBalanceInput('5');
        expect(dWTransactionUpdatePage.getAvailableBalanceInput()).toMatch('5');
        dWTransactionUpdatePage.save();
        expect(dWTransactionUpdatePage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});
