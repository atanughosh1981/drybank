package com.mck.drybank.account.web.rest;

import com.mck.drybank.account.DrybankApp;

import com.mck.drybank.account.domain.DWTransaction;
import com.mck.drybank.account.repository.DWTransactionRepository;
import com.mck.drybank.account.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.mck.drybank.account.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mck.drybank.account.domain.enumeration.TRANSTYPE;
/**
 * Test class for the DWTransactionResource REST controller.
 *
 * @see DWTransactionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DrybankApp.class)
public class DWTransactionResourceIntTest {

    private static final LocalDate DEFAULT_TRANSACTION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TRANSACTION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_TRANSACTION_REMARK = "AAAAAAAAAA";
    private static final String UPDATED_TRANSACTION_REMARK = "BBBBBBBBBB";

    private static final TRANSTYPE DEFAULT_TRANSACTION_TYPE = TRANSTYPE.CREDIT;
    private static final TRANSTYPE UPDATED_TRANSACTION_TYPE = TRANSTYPE.DEBIT;

    private static final Double DEFAULT_AVAILABLE_BALANCE = 1D;
    private static final Double UPDATED_AVAILABLE_BALANCE = 2D;

    @Autowired
    private DWTransactionRepository dWTransactionRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDWTransactionMockMvc;

    private DWTransaction dWTransaction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DWTransactionResource dWTransactionResource = new DWTransactionResource(dWTransactionRepository);
        this.restDWTransactionMockMvc = MockMvcBuilders.standaloneSetup(dWTransactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DWTransaction createEntity(EntityManager em) {
        DWTransaction dWTransaction = new DWTransaction()
            .transactionDate(DEFAULT_TRANSACTION_DATE)
            .transactionRemark(DEFAULT_TRANSACTION_REMARK)
            .transactionType(DEFAULT_TRANSACTION_TYPE)
            .availableBalance(DEFAULT_AVAILABLE_BALANCE);
        return dWTransaction;
    }

    @Before
    public void initTest() {
        dWTransaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createDWTransaction() throws Exception {
        int databaseSizeBeforeCreate = dWTransactionRepository.findAll().size();

        // Create the DWTransaction
        restDWTransactionMockMvc.perform(post("/api/dw-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWTransaction)))
            .andExpect(status().isCreated());

        // Validate the DWTransaction in the database
        List<DWTransaction> dWTransactionList = dWTransactionRepository.findAll();
        assertThat(dWTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        DWTransaction testDWTransaction = dWTransactionList.get(dWTransactionList.size() - 1);
        assertThat(testDWTransaction.getTransactionDate()).isEqualTo(DEFAULT_TRANSACTION_DATE);
        assertThat(testDWTransaction.getTransactionRemark()).isEqualTo(DEFAULT_TRANSACTION_REMARK);
        assertThat(testDWTransaction.getTransactionType()).isEqualTo(DEFAULT_TRANSACTION_TYPE);
        assertThat(testDWTransaction.getAvailableBalance()).isEqualTo(DEFAULT_AVAILABLE_BALANCE);
    }

    @Test
    @Transactional
    public void createDWTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dWTransactionRepository.findAll().size();

        // Create the DWTransaction with an existing ID
        dWTransaction.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDWTransactionMockMvc.perform(post("/api/dw-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWTransaction)))
            .andExpect(status().isBadRequest());

        // Validate the DWTransaction in the database
        List<DWTransaction> dWTransactionList = dWTransactionRepository.findAll();
        assertThat(dWTransactionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTransactionDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = dWTransactionRepository.findAll().size();
        // set the field null
        dWTransaction.setTransactionDate(null);

        // Create the DWTransaction, which fails.

        restDWTransactionMockMvc.perform(post("/api/dw-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWTransaction)))
            .andExpect(status().isBadRequest());

        List<DWTransaction> dWTransactionList = dWTransactionRepository.findAll();
        assertThat(dWTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTransactionRemarkIsRequired() throws Exception {
        int databaseSizeBeforeTest = dWTransactionRepository.findAll().size();
        // set the field null
        dWTransaction.setTransactionRemark(null);

        // Create the DWTransaction, which fails.

        restDWTransactionMockMvc.perform(post("/api/dw-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWTransaction)))
            .andExpect(status().isBadRequest());

        List<DWTransaction> dWTransactionList = dWTransactionRepository.findAll();
        assertThat(dWTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTransactionTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = dWTransactionRepository.findAll().size();
        // set the field null
        dWTransaction.setTransactionType(null);

        // Create the DWTransaction, which fails.

        restDWTransactionMockMvc.perform(post("/api/dw-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWTransaction)))
            .andExpect(status().isBadRequest());

        List<DWTransaction> dWTransactionList = dWTransactionRepository.findAll();
        assertThat(dWTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAvailableBalanceIsRequired() throws Exception {
        int databaseSizeBeforeTest = dWTransactionRepository.findAll().size();
        // set the field null
        dWTransaction.setAvailableBalance(null);

        // Create the DWTransaction, which fails.

        restDWTransactionMockMvc.perform(post("/api/dw-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWTransaction)))
            .andExpect(status().isBadRequest());

        List<DWTransaction> dWTransactionList = dWTransactionRepository.findAll();
        assertThat(dWTransactionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDWTransactions() throws Exception {
        // Initialize the database
        dWTransactionRepository.saveAndFlush(dWTransaction);

        // Get all the dWTransactionList
        restDWTransactionMockMvc.perform(get("/api/dw-transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dWTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].transactionDate").value(hasItem(DEFAULT_TRANSACTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].transactionRemark").value(hasItem(DEFAULT_TRANSACTION_REMARK.toString())))
            .andExpect(jsonPath("$.[*].transactionType").value(hasItem(DEFAULT_TRANSACTION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].availableBalance").value(hasItem(DEFAULT_AVAILABLE_BALANCE.doubleValue())));
    }
    

    @Test
    @Transactional
    public void getDWTransaction() throws Exception {
        // Initialize the database
        dWTransactionRepository.saveAndFlush(dWTransaction);

        // Get the dWTransaction
        restDWTransactionMockMvc.perform(get("/api/dw-transactions/{id}", dWTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dWTransaction.getId().intValue()))
            .andExpect(jsonPath("$.transactionDate").value(DEFAULT_TRANSACTION_DATE.toString()))
            .andExpect(jsonPath("$.transactionRemark").value(DEFAULT_TRANSACTION_REMARK.toString()))
            .andExpect(jsonPath("$.transactionType").value(DEFAULT_TRANSACTION_TYPE.toString()))
            .andExpect(jsonPath("$.availableBalance").value(DEFAULT_AVAILABLE_BALANCE.doubleValue()));
    }
    @Test
    @Transactional
    public void getNonExistingDWTransaction() throws Exception {
        // Get the dWTransaction
        restDWTransactionMockMvc.perform(get("/api/dw-transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDWTransaction() throws Exception {
        // Initialize the database
        dWTransactionRepository.saveAndFlush(dWTransaction);

        int databaseSizeBeforeUpdate = dWTransactionRepository.findAll().size();

        // Update the dWTransaction
        DWTransaction updatedDWTransaction = dWTransactionRepository.findById(dWTransaction.getId()).get();
        // Disconnect from session so that the updates on updatedDWTransaction are not directly saved in db
        em.detach(updatedDWTransaction);
        updatedDWTransaction
            .transactionDate(UPDATED_TRANSACTION_DATE)
            .transactionRemark(UPDATED_TRANSACTION_REMARK)
            .transactionType(UPDATED_TRANSACTION_TYPE)
            .availableBalance(UPDATED_AVAILABLE_BALANCE);

        restDWTransactionMockMvc.perform(put("/api/dw-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDWTransaction)))
            .andExpect(status().isOk());

        // Validate the DWTransaction in the database
        List<DWTransaction> dWTransactionList = dWTransactionRepository.findAll();
        assertThat(dWTransactionList).hasSize(databaseSizeBeforeUpdate);
        DWTransaction testDWTransaction = dWTransactionList.get(dWTransactionList.size() - 1);
        assertThat(testDWTransaction.getTransactionDate()).isEqualTo(UPDATED_TRANSACTION_DATE);
        assertThat(testDWTransaction.getTransactionRemark()).isEqualTo(UPDATED_TRANSACTION_REMARK);
        assertThat(testDWTransaction.getTransactionType()).isEqualTo(UPDATED_TRANSACTION_TYPE);
        assertThat(testDWTransaction.getAvailableBalance()).isEqualTo(UPDATED_AVAILABLE_BALANCE);
    }

    @Test
    @Transactional
    public void updateNonExistingDWTransaction() throws Exception {
        int databaseSizeBeforeUpdate = dWTransactionRepository.findAll().size();

        // Create the DWTransaction

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDWTransactionMockMvc.perform(put("/api/dw-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dWTransaction)))
            .andExpect(status().isBadRequest());

        // Validate the DWTransaction in the database
        List<DWTransaction> dWTransactionList = dWTransactionRepository.findAll();
        assertThat(dWTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDWTransaction() throws Exception {
        // Initialize the database
        dWTransactionRepository.saveAndFlush(dWTransaction);

        int databaseSizeBeforeDelete = dWTransactionRepository.findAll().size();

        // Get the dWTransaction
        restDWTransactionMockMvc.perform(delete("/api/dw-transactions/{id}", dWTransaction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DWTransaction> dWTransactionList = dWTransactionRepository.findAll();
        assertThat(dWTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DWTransaction.class);
        DWTransaction dWTransaction1 = new DWTransaction();
        dWTransaction1.setId(1L);
        DWTransaction dWTransaction2 = new DWTransaction();
        dWTransaction2.setId(dWTransaction1.getId());
        assertThat(dWTransaction1).isEqualTo(dWTransaction2);
        dWTransaction2.setId(2L);
        assertThat(dWTransaction1).isNotEqualTo(dWTransaction2);
        dWTransaction1.setId(null);
        assertThat(dWTransaction1).isNotEqualTo(dWTransaction2);
    }
}
