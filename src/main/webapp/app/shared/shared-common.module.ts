import { NgModule } from '@angular/core';

import { DrybankSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [DrybankSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [DrybankSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class DrybankSharedCommonModule {}
