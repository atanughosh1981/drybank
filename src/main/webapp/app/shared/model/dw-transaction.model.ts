import { Moment } from 'moment';

export const enum TRANSTYPE {
    CREDIT = 'CREDIT',
    DEBIT = 'DEBIT'
}

export interface IDWTransaction {
    id?: number;
    transactionDate?: Moment;
    transactionRemark?: string;
    transactionType?: TRANSTYPE;
    availableBalance?: number;
}

export class DWTransaction implements IDWTransaction {
    constructor(
        public id?: number,
        public transactionDate?: Moment,
        public transactionRemark?: string,
        public transactionType?: TRANSTYPE,
        public availableBalance?: number
    ) {}
}
