export interface IDWBankAccount {
    id?: number;
    username?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    phone?: string;
    address?: string;
    postalCode?: string;
    panNumber?: string;
}

export class DWBankAccount implements IDWBankAccount {
    constructor(
        public id?: number,
        public username?: string,
        public password?: string,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public phone?: string,
        public address?: string,
        public postalCode?: string,
        public panNumber?: string
    ) {}
}
