import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DrybankSharedModule } from 'app/shared';
import {
    DWBankAccountComponent,
    DWBankAccountDetailComponent,
    DWBankAccountUpdateComponent,
    DWBankAccountDeletePopupComponent,
    DWBankAccountDeleteDialogComponent,
    dWBankAccountRoute,
    dWBankAccountPopupRoute
} from './';

const ENTITY_STATES = [...dWBankAccountRoute, ...dWBankAccountPopupRoute];

@NgModule({
    imports: [DrybankSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        DWBankAccountComponent,
        DWBankAccountDetailComponent,
        DWBankAccountUpdateComponent,
        DWBankAccountDeleteDialogComponent,
        DWBankAccountDeletePopupComponent
    ],
    entryComponents: [
        DWBankAccountComponent,
        DWBankAccountUpdateComponent,
        DWBankAccountDeleteDialogComponent,
        DWBankAccountDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DrybankDWBankAccountModule {}
