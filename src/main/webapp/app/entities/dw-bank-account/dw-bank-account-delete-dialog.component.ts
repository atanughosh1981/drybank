import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDWBankAccount } from 'app/shared/model/dw-bank-account.model';
import { DWBankAccountService } from './dw-bank-account.service';

@Component({
    selector: 'jhi-dw-bank-account-delete-dialog',
    templateUrl: './dw-bank-account-delete-dialog.component.html'
})
export class DWBankAccountDeleteDialogComponent {
    dWBankAccount: IDWBankAccount;

    constructor(
        private dWBankAccountService: DWBankAccountService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dWBankAccountService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'dWBankAccountListModification',
                content: 'Deleted an dWBankAccount'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-dw-bank-account-delete-popup',
    template: ''
})
export class DWBankAccountDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ dWBankAccount }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(DWBankAccountDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.dWBankAccount = dWBankAccount;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
