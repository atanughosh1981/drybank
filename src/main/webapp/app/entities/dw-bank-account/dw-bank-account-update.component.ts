import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IDWBankAccount } from 'app/shared/model/dw-bank-account.model';
import { DWBankAccountService } from './dw-bank-account.service';

@Component({
    selector: 'jhi-dw-bank-account-update',
    templateUrl: './dw-bank-account-update.component.html'
})
export class DWBankAccountUpdateComponent implements OnInit {
    private _dWBankAccount: IDWBankAccount;
    isSaving: boolean;

    constructor(private dWBankAccountService: DWBankAccountService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ dWBankAccount }) => {
            this.dWBankAccount = dWBankAccount;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.dWBankAccount.id !== undefined) {
            this.subscribeToSaveResponse(this.dWBankAccountService.update(this.dWBankAccount));
        } else {
            this.subscribeToSaveResponse(this.dWBankAccountService.create(this.dWBankAccount));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IDWBankAccount>>) {
        result.subscribe((res: HttpResponse<IDWBankAccount>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get dWBankAccount() {
        return this._dWBankAccount;
    }

    set dWBankAccount(dWBankAccount: IDWBankAccount) {
        this._dWBankAccount = dWBankAccount;
    }
}
