import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDWBankAccount } from 'app/shared/model/dw-bank-account.model';

type EntityResponseType = HttpResponse<IDWBankAccount>;
type EntityArrayResponseType = HttpResponse<IDWBankAccount[]>;

@Injectable({ providedIn: 'root' })
export class DWBankAccountService {
    private resourceUrl = SERVER_API_URL + 'api/dw-bank-accounts';

    constructor(private http: HttpClient) {}

    create(dWBankAccount: IDWBankAccount): Observable<EntityResponseType> {
        return this.http.post<IDWBankAccount>(this.resourceUrl, dWBankAccount, { observe: 'response' });
    }

    update(dWBankAccount: IDWBankAccount): Observable<EntityResponseType> {
        return this.http.put<IDWBankAccount>(this.resourceUrl, dWBankAccount, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IDWBankAccount>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IDWBankAccount[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
