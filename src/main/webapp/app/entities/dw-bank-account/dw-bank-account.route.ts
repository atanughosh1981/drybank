import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable } from 'rxjs';
import { DWBankAccount } from 'app/shared/model/dw-bank-account.model';
import { DWBankAccountService } from './dw-bank-account.service';
import { DWBankAccountComponent } from './dw-bank-account.component';
import { DWBankAccountDetailComponent } from './dw-bank-account-detail.component';
import { DWBankAccountUpdateComponent } from './dw-bank-account-update.component';
import { DWBankAccountDeletePopupComponent } from './dw-bank-account-delete-dialog.component';
import { IDWBankAccount } from 'app/shared/model/dw-bank-account.model';

@Injectable({ providedIn: 'root' })
export class DWBankAccountResolve implements Resolve<IDWBankAccount> {
    constructor(private service: DWBankAccountService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).map((dWBankAccount: HttpResponse<DWBankAccount>) => dWBankAccount.body);
        }
        return Observable.of(new DWBankAccount());
    }
}

export const dWBankAccountRoute: Routes = [
    {
        path: 'dw-bank-account',
        component: DWBankAccountComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DWBankAccounts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dw-bank-account/:id/view',
        component: DWBankAccountDetailComponent,
        resolve: {
            dWBankAccount: DWBankAccountResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DWBankAccounts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dw-bank-account/new',
        component: DWBankAccountUpdateComponent,
        resolve: {
            dWBankAccount: DWBankAccountResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DWBankAccounts'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dw-bank-account/:id/edit',
        component: DWBankAccountUpdateComponent,
        resolve: {
            dWBankAccount: DWBankAccountResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DWBankAccounts'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dWBankAccountPopupRoute: Routes = [
    {
        path: 'dw-bank-account/:id/delete',
        component: DWBankAccountDeletePopupComponent,
        resolve: {
            dWBankAccount: DWBankAccountResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DWBankAccounts'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
