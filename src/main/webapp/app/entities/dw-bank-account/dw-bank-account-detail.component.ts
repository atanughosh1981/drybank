import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDWBankAccount } from 'app/shared/model/dw-bank-account.model';

@Component({
    selector: 'jhi-dw-bank-account-detail',
    templateUrl: './dw-bank-account-detail.component.html'
})
export class DWBankAccountDetailComponent implements OnInit {
    dWBankAccount: IDWBankAccount;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ dWBankAccount }) => {
            this.dWBankAccount = dWBankAccount;
        });
    }

    previousState() {
        window.history.back();
    }
}
