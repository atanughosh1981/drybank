import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IDWBankAccount } from 'app/shared/model/dw-bank-account.model';
import { Principal } from 'app/core';
import { DWBankAccountService } from './dw-bank-account.service';

@Component({
    selector: 'jhi-dw-bank-account',
    templateUrl: './dw-bank-account.component.html'
})
export class DWBankAccountComponent implements OnInit, OnDestroy {
    dWBankAccounts: IDWBankAccount[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private dWBankAccountService: DWBankAccountService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.dWBankAccountService.query().subscribe(
            (res: HttpResponse<IDWBankAccount[]>) => {
                this.dWBankAccounts = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInDWBankAccounts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IDWBankAccount) {
        return item.id;
    }

    registerChangeInDWBankAccounts() {
        this.eventSubscriber = this.eventManager.subscribe('dWBankAccountListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
