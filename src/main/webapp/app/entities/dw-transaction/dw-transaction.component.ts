import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IDWTransaction } from 'app/shared/model/dw-transaction.model';
import { Principal } from 'app/core';
import { DWTransactionService } from './dw-transaction.service';

@Component({
    selector: 'jhi-dw-transaction',
    templateUrl: './dw-transaction.component.html'
})
export class DWTransactionComponent implements OnInit, OnDestroy {
    dWTransactions: IDWTransaction[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private dWTransactionService: DWTransactionService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.dWTransactionService.query().subscribe(
            (res: HttpResponse<IDWTransaction[]>) => {
                this.dWTransactions = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInDWTransactions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IDWTransaction) {
        return item.id;
    }

    registerChangeInDWTransactions() {
        this.eventSubscriber = this.eventManager.subscribe('dWTransactionListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
