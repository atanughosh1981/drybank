import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IDWTransaction } from 'app/shared/model/dw-transaction.model';

type EntityResponseType = HttpResponse<IDWTransaction>;
type EntityArrayResponseType = HttpResponse<IDWTransaction[]>;

@Injectable({ providedIn: 'root' })
export class DWTransactionService {
    private resourceUrl = SERVER_API_URL + 'api/dw-transactions';

    constructor(private http: HttpClient) {}

    create(dWTransaction: IDWTransaction): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(dWTransaction);
        return this.http
            .post<IDWTransaction>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertDateFromServer(res));
    }

    update(dWTransaction: IDWTransaction): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(dWTransaction);
        return this.http
            .put<IDWTransaction>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertDateFromServer(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IDWTransaction>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertDateFromServer(res));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IDWTransaction[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(dWTransaction: IDWTransaction): IDWTransaction {
        const copy: IDWTransaction = Object.assign({}, dWTransaction, {
            transactionDate:
                dWTransaction.transactionDate != null && dWTransaction.transactionDate.isValid()
                    ? dWTransaction.transactionDate.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.transactionDate = res.body.transactionDate != null ? moment(res.body.transactionDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((dWTransaction: IDWTransaction) => {
            dWTransaction.transactionDate = dWTransaction.transactionDate != null ? moment(dWTransaction.transactionDate) : null;
        });
        return res;
    }
}
