import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDWTransaction } from 'app/shared/model/dw-transaction.model';

@Component({
    selector: 'jhi-dw-transaction-detail',
    templateUrl: './dw-transaction-detail.component.html'
})
export class DWTransactionDetailComponent implements OnInit {
    dWTransaction: IDWTransaction;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ dWTransaction }) => {
            this.dWTransaction = dWTransaction;
        });
    }

    previousState() {
        window.history.back();
    }
}
