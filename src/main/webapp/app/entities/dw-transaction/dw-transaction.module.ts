import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DrybankSharedModule } from 'app/shared';
import {
    DWTransactionComponent,
    DWTransactionDetailComponent,
    DWTransactionUpdateComponent,
    DWTransactionDeletePopupComponent,
    DWTransactionDeleteDialogComponent,
    dWTransactionRoute,
    dWTransactionPopupRoute
} from './';

const ENTITY_STATES = [...dWTransactionRoute, ...dWTransactionPopupRoute];

@NgModule({
    imports: [DrybankSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        DWTransactionComponent,
        DWTransactionDetailComponent,
        DWTransactionUpdateComponent,
        DWTransactionDeleteDialogComponent,
        DWTransactionDeletePopupComponent
    ],
    entryComponents: [
        DWTransactionComponent,
        DWTransactionUpdateComponent,
        DWTransactionDeleteDialogComponent,
        DWTransactionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DrybankDWTransactionModule {}
