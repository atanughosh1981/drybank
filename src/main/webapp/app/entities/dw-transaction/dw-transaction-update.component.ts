import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IDWTransaction } from 'app/shared/model/dw-transaction.model';
import { DWTransactionService } from './dw-transaction.service';

@Component({
    selector: 'jhi-dw-transaction-update',
    templateUrl: './dw-transaction-update.component.html'
})
export class DWTransactionUpdateComponent implements OnInit {
    private _dWTransaction: IDWTransaction;
    isSaving: boolean;
    transactionDateDp: any;

    constructor(private dWTransactionService: DWTransactionService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ dWTransaction }) => {
            this.dWTransaction = dWTransaction;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.dWTransaction.id !== undefined) {
            this.subscribeToSaveResponse(this.dWTransactionService.update(this.dWTransaction));
        } else {
            this.subscribeToSaveResponse(this.dWTransactionService.create(this.dWTransaction));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IDWTransaction>>) {
        result.subscribe((res: HttpResponse<IDWTransaction>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
    get dWTransaction() {
        return this._dWTransaction;
    }

    set dWTransaction(dWTransaction: IDWTransaction) {
        this._dWTransaction = dWTransaction;
    }
}
