import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IDWTransaction } from 'app/shared/model/dw-transaction.model';
import { DWTransactionService } from './dw-transaction.service';

@Component({
    selector: 'jhi-dw-transaction-delete-dialog',
    templateUrl: './dw-transaction-delete-dialog.component.html'
})
export class DWTransactionDeleteDialogComponent {
    dWTransaction: IDWTransaction;

    constructor(
        private dWTransactionService: DWTransactionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dWTransactionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'dWTransactionListModification',
                content: 'Deleted an dWTransaction'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-dw-transaction-delete-popup',
    template: ''
})
export class DWTransactionDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ dWTransaction }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(DWTransactionDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.dWTransaction = dWTransaction;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
