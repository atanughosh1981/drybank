import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable } from 'rxjs';
import { DWTransaction } from 'app/shared/model/dw-transaction.model';
import { DWTransactionService } from './dw-transaction.service';
import { DWTransactionComponent } from './dw-transaction.component';
import { DWTransactionDetailComponent } from './dw-transaction-detail.component';
import { DWTransactionUpdateComponent } from './dw-transaction-update.component';
import { DWTransactionDeletePopupComponent } from './dw-transaction-delete-dialog.component';
import { IDWTransaction } from 'app/shared/model/dw-transaction.model';

@Injectable({ providedIn: 'root' })
export class DWTransactionResolve implements Resolve<IDWTransaction> {
    constructor(private service: DWTransactionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).map((dWTransaction: HttpResponse<DWTransaction>) => dWTransaction.body);
        }
        return Observable.of(new DWTransaction());
    }
}

export const dWTransactionRoute: Routes = [
    {
        path: 'dw-transaction',
        component: DWTransactionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DWTransactions'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dw-transaction/:id/view',
        component: DWTransactionDetailComponent,
        resolve: {
            dWTransaction: DWTransactionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DWTransactions'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dw-transaction/new',
        component: DWTransactionUpdateComponent,
        resolve: {
            dWTransaction: DWTransactionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DWTransactions'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'dw-transaction/:id/edit',
        component: DWTransactionUpdateComponent,
        resolve: {
            dWTransaction: DWTransactionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DWTransactions'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dWTransactionPopupRoute: Routes = [
    {
        path: 'dw-transaction/:id/delete',
        component: DWTransactionDeletePopupComponent,
        resolve: {
            dWTransaction: DWTransactionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'DWTransactions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
