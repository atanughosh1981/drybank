import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { DrybankDWBankAccountModule } from './dw-bank-account/dw-bank-account.module';
import { DrybankDWTransactionModule } from './dw-transaction/dw-transaction.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        DrybankDWBankAccountModule,
        DrybankDWTransactionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DrybankEntityModule {}
