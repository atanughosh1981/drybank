package com.mck.drybank.account.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import com.mck.drybank.account.domain.enumeration.TRANSTYPE;

/**
 * A DWTransaction.
 */
@Entity
@Table(name = "dw_transaction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DWTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "transaction_date", nullable = false)
    private LocalDate transactionDate;

    @NotNull
    @Column(name = "transaction_remark", nullable = false)
    private String transactionRemark;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type", nullable = false)
    private TRANSTYPE transactionType;

    @NotNull
    @Column(name = "available_balance", nullable = false)
    private Double availableBalance;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public DWTransaction transactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    public void setTransactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionRemark() {
        return transactionRemark;
    }

    public DWTransaction transactionRemark(String transactionRemark) {
        this.transactionRemark = transactionRemark;
        return this;
    }

    public void setTransactionRemark(String transactionRemark) {
        this.transactionRemark = transactionRemark;
    }

    public TRANSTYPE getTransactionType() {
        return transactionType;
    }

    public DWTransaction transactionType(TRANSTYPE transactionType) {
        this.transactionType = transactionType;
        return this;
    }

    public void setTransactionType(TRANSTYPE transactionType) {
        this.transactionType = transactionType;
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public DWTransaction availableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
        return this;
    }

    public void setAvailableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DWTransaction dWTransaction = (DWTransaction) o;
        if (dWTransaction.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dWTransaction.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DWTransaction{" +
            "id=" + getId() +
            ", transactionDate='" + getTransactionDate() + "'" +
            ", transactionRemark='" + getTransactionRemark() + "'" +
            ", transactionType='" + getTransactionType() + "'" +
            ", availableBalance=" + getAvailableBalance() +
            "}";
    }
}
