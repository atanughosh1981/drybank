package com.mck.drybank.account.domain.enumeration;

/**
 * The TRANSTYPE enumeration.
 */
public enum TRANSTYPE {
    CREDIT, DEBIT
}
