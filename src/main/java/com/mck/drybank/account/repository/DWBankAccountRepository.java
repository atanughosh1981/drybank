package com.mck.drybank.account.repository;

import com.mck.drybank.account.domain.DWBankAccount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DWBankAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DWBankAccountRepository extends JpaRepository<DWBankAccount, Long> {

}
