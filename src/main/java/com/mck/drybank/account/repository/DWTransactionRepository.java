package com.mck.drybank.account.repository;

import com.mck.drybank.account.domain.DWTransaction;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DWTransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DWTransactionRepository extends JpaRepository<DWTransaction, Long> {

}
