/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mck.drybank.account.web.rest.vm;
