package com.mck.drybank.account.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mck.drybank.account.domain.DWTransaction;
import com.mck.drybank.account.repository.DWTransactionRepository;
import com.mck.drybank.account.web.rest.errors.BadRequestAlertException;
import com.mck.drybank.account.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DWTransaction.
 */
@RestController
@RequestMapping("/api")
public class DWTransactionResource {

    private final Logger log = LoggerFactory.getLogger(DWTransactionResource.class);

    private static final String ENTITY_NAME = "dWTransaction";

    private final DWTransactionRepository dWTransactionRepository;

    public DWTransactionResource(DWTransactionRepository dWTransactionRepository) {
        this.dWTransactionRepository = dWTransactionRepository;
    }

    /**
     * POST  /dw-transactions : Create a new dWTransaction.
     *
     * @param dWTransaction the dWTransaction to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dWTransaction, or with status 400 (Bad Request) if the dWTransaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dw-transactions")
    @Timed
    public ResponseEntity<DWTransaction> createDWTransaction(@Valid @RequestBody DWTransaction dWTransaction) throws URISyntaxException {
        log.debug("REST request to save DWTransaction : {}", dWTransaction);
        if (dWTransaction.getId() != null) {
            throw new BadRequestAlertException("A new dWTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DWTransaction result = dWTransactionRepository.save(dWTransaction);
        return ResponseEntity.created(new URI("/api/dw-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dw-transactions : Updates an existing dWTransaction.
     *
     * @param dWTransaction the dWTransaction to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dWTransaction,
     * or with status 400 (Bad Request) if the dWTransaction is not valid,
     * or with status 500 (Internal Server Error) if the dWTransaction couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dw-transactions")
    @Timed
    public ResponseEntity<DWTransaction> updateDWTransaction(@Valid @RequestBody DWTransaction dWTransaction) throws URISyntaxException {
        log.debug("REST request to update DWTransaction : {}", dWTransaction);
        if (dWTransaction.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DWTransaction result = dWTransactionRepository.save(dWTransaction);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dWTransaction.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dw-transactions : get all the dWTransactions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dWTransactions in body
     */
    @GetMapping("/dw-transactions")
    @Timed
    public List<DWTransaction> getAllDWTransactions() {
        log.debug("REST request to get all DWTransactions");
        return dWTransactionRepository.findAll();
    }

    /**
     * GET  /dw-transactions/:id : get the "id" dWTransaction.
     *
     * @param id the id of the dWTransaction to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dWTransaction, or with status 404 (Not Found)
     */
    @GetMapping("/dw-transactions/{id}")
    @Timed
    public ResponseEntity<DWTransaction> getDWTransaction(@PathVariable Long id) {
        log.debug("REST request to get DWTransaction : {}", id);
        Optional<DWTransaction> dWTransaction = dWTransactionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(dWTransaction);
    }

    /**
     * DELETE  /dw-transactions/:id : delete the "id" dWTransaction.
     *
     * @param id the id of the dWTransaction to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dw-transactions/{id}")
    @Timed
    public ResponseEntity<Void> deleteDWTransaction(@PathVariable Long id) {
        log.debug("REST request to delete DWTransaction : {}", id);

        dWTransactionRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
